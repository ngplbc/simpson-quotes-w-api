import React from "react";

const Quote = ({ quoteObj }) => {
  const { quote, character, image } = quoteObj;
  return (
    <>
      <img src={image} alt=""></img>
      <h3>{quote}</h3>
      <p>{character}</p>
    </>
  )
};

export default Quote;