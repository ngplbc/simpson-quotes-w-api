import React from "react";
import "./App.css";
import Quote from "../Quote/Quote";
import { getQuoteHelper } from "../../util/getHelper";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quoteObj: {}
    }
  }

  componentDidMount = () => {
    this.getQuote()
  }

  getQuote = async () => {
    const quoteObj = await getQuoteHelper();
    this.setState({
      quoteObj: quoteObj
    })
  };

  render() {
    console.log(this.state.quoteObj)
    return (
      <div className="App">
        {this.state.quoteObj.error ? <h2>{this.state.quoteObj.error}</h2> : <Quote quoteObj={this.state.quoteObj}/>}
        <button onClick={this.getQuote}>Get Quote</button>      
      </div>
    );
  }
}

export default App;
