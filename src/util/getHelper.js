const getQuoteHelper = async () => {
  try {
    const rawResponse = await fetch(`https://thesimpsonsquoteapi.glitch.me/quotes`);
    const response = await rawResponse.json();
    return response[0];

  } catch(e) {
    // console.log(e.message);
    return { error: e.message };
  }
}

export { getQuoteHelper };